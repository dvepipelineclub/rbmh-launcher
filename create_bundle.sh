rm -Rf build/
rm *.pyc

#export PYTHONPATH=/Users/andreas.gutsche/Desktop/testpyenv/:$PYTHONPATH
python setup.py py2app
#--frameworks qt
pushd dist/Adobe\ Premiere\ Profile\ Sync.app/Contents/Resources/lib/python2.7/

unzip site-packages.zip -d ./site-packages
rm site-packages.zip
#cp -R ../../../../../../libs_pyside/PySide site-packages/
cp -R ../../../../../../libs/passlib site-packages/
cp ../../../../../../libs/six.py* site-packages/
cp ../../../../../../red-bull-media-house-logo.png site-packages/
cp ../../../../../../configuration.json site-packages/
popd
