"""
This module contains all the UI specific setup and callbacks. Any modifications
related to the UI should be done in here.
"""
import os

from PySide import QtCore, QtGui
from sqlalchemy.orm import sessionmaker

from authentication import initialize_db, User, authenticate

def show_error_message(title, error_msg):
    msgBox = QtGui.QMessageBox()
    msgBox.setText(title)
    msgBox.setIcon(QtGui.QMessageBox.Warning)
    msgBox.setInformativeText(error_msg)
    msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
    msgBox.setDefaultButton(QtGui.QMessageBox.Ok)
    ret = msgBox.exec_()

class LauncherWindow(QtGui.QMainWindow):
    """The Main Application Window. Closes itself once the application was launched."""
    def __init__(self, configuration, parent=None):
        """Create the individual widgets used in the window and nest them in
        layouts and groupings. Then calls setup methods for:
        * setting up the menubar actions (should not clutter main UI)
        * initial filling up of the user list
        * setting up triggering connections ( signals / slots )"""
        super(LauncherWindow,self).__init__(parent)
        self.success = False
        self.configuration = configuration
        
        main_lyt = QtGui.QVBoxLayout()
        main_wid = QtGui.QWidget()
        self.setCentralWidget(main_wid)
        main_wid.setLayout(main_lyt)
        
        # Login Widgets
        self.user_cbox = QtGui.QComboBox()
        self.password = QtGui.QLineEdit()
        self.password.setPlaceholderText("<enter password>")
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        
        self.message_box = QtGui.QLabel("Please enter your credentials")
        self.message_box.setAlignment(QtCore.Qt.AlignCenter)
        self.message_box.setTextFormat(QtCore.Qt.RichText)
        self.login_btn = QtGui.QPushButton("Login")
        
        # User-Settings
        self.forgot_btn = QtGui.QPushButton()
        self.change_passw_btn = QtGui.QPushButton()

        # Styling
        current_user_header = QtGui.QWidget()
        self.current_label = QtGui.QLabel("currently logged in user: ")
        self.current_user = QtGui.QLabel()
        self.current_user.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.current_label.setStyleSheet("color: #00AA00")
        self.current_user.setStyleSheet("font-weight: bold")

        current_lyt = QtGui.QHBoxLayout(current_user_header)
        self.log_out_btn = QtGui.QPushButton("Log out")
        self.log_out_btn.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        current_lyt.addWidget(self.current_label)
        current_lyt.addWidget(self.current_user)
        current_lyt.addWidget(self.log_out_btn)

        rbmh_label = QtGui.QLabel("Adobe Premiere Profile Sync")
        self.setWindowTitle(rbmh_label.text())
        rbmh_label.setPixmap(QtGui.QPixmap("./red-bull-media-house-logo.png"))
        rbmh_label.setAlignment(QtCore.Qt.AlignCenter)
        main_lyt.addWidget(current_user_header)
        main_lyt.addWidget(rbmh_label)
        
        # setup layout and add widgets
        login_frm = QtGui.QFrame()
        login_lyt = QtGui.QHBoxLayout()
        login_frm.setLayout(login_lyt)
        login_lyt.addWidget(self.user_cbox)
        login_lyt.addWidget(self.password)
        login_lyt.addWidget(self.login_btn)
        main_lyt.addWidget(self.message_box)
        main_lyt.addWidget(login_frm)
        
        # setup menubar items containing administrative functions
        self.setup_menubar()
        
        self.whois_logged_in()
        self.init_database()
        self.fill_users(self.user_cbox)
        self.setup_connections()
        
    def setup_menubar(self):
        """Sets up the add/remove user menu as well as the help menu."""
        menu = QtGui.QMenuBar()
        user_menu = QtGui.QMenu("Users")
        backup_menu = QtGui.QMenu("Profile Management")
        password_menu = QtGui.QMenu("Password")
        help_menu = QtGui.QMenu("Help")
        self.logged_in_menu = QtGui.QMenu("")
        #self.logged_in_menu.setIcon(QtGui.QIcon("./user.png"))

        self.about_ac = QtGui.QAction("About", self)
        self.show_help_ac = QtGui.QAction("Show Help", self)
        help_menu.addAction(self.about_ac)
        help_menu.addAction(self.show_help_ac)

        self.adduser_ac = QtGui.QAction("Add User", self)
        self.removeuser_ac = QtGui.QAction("Remove Users", self)
        self.company_login_ac = QtGui.QAction("Log In as Company User", self)
        user_menu.addAction(self.adduser_ac)
        user_menu.addAction(self.removeuser_ac)
        user_menu.addAction(self.company_login_ac)

        self.backup_settings_ac = QtGui.QAction("Backup Current Profile", self)
        self.restore_settings_ac = QtGui.QAction("Overwrite Profile on Server", self)
        self.reset_settings_ac = QtGui.QAction("Reset Current Profile to default", self)
        backup_menu.addAction(self.backup_settings_ac)
        backup_menu.addAction(self.restore_settings_ac)
        backup_menu.addAction(self.reset_settings_ac)

        
        menu.addMenu(user_menu)
        menu.addMenu(backup_menu)
        menu.addMenu(self.logged_in_menu)
        #menu.addMenu(password_menu)
        #menu.addMenu(help_menu)
        
        self.setMenuBar(menu)
        
    def init_database(self):
        """updates the user list combo box from entries in the database"""
        engine = initialize_db()
        Session = sessionmaker(bind=engine)
        self.session = Session()        # the session to be used throughout
                                        # this class
    
    def fill_users(self, widget):
        user_list = list()
        widget.clear()
        for user in self.session.query(User).all():
            user_list.append(user.login_name)
            widget.addItem("%s %s (%s)" % (user.first_name, 
                                                   user.last_name, 
                                                   user.login_name
                                                  ), user.login_name)
        completer = QtGui.QCompleter(user_list)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        widget.setCompleter(completer)

    def setup_connections(self):
        self.login_btn.clicked.connect(self.login_user)
        self.log_out_btn.clicked.connect(self.logout_user)
        self.password.returnPressed.connect(self.login_btn.click)
        self.adduser_ac.triggered.connect(self.add_user)
        self.removeuser_ac.triggered.connect(self.remove_user)
        self.company_login_ac.triggered.connect(self.company_login)
        self.backup_settings_ac.triggered.connect(self.backup_settings)
        self.restore_settings_ac.triggered.connect(self.restore_settings)
        self.reset_settings_ac.triggered.connect(self.reset_settings)
        
    def add_user(self):
        """Pops up a dialog to add new user entries"""
        admin_dlg = AdminDialog(self.configuration, self)
        admin_dlg.setModal(True)
        admin_ok = admin_dlg.exec_()
        if not admin_dlg.authenticated:
            return

        adduser_dlg = AddUserDialog(self.session, self)
        adduser_dlg.setModal(True)
        return_value = adduser_dlg.exec_()
        if return_value:
            user_fields = adduser_dlg.return_user()
            self.session.add(User(**user_fields))
            self.session.commit()
            self.fill_users(self.user_cbox)

    def remove_user(self):
        admin_dlg = AdminDialog(self.configuration, self)
        admin_dlg.setModal(True)
        admin_ok = admin_dlg.exec_()
        if not admin_dlg.authenticated:
            return

        removeuser_dlg = RemoveUserDialog(self.session, self)
        removeuser_dlg.setModal(True)
        removeuser_dlg.exec_()
        self.fill_users(self.user_cbox)
        
    def login_user(self):
        """Attempts to authenticate the user. If this succeeds, the application
        settings are synchronized and the application launched"""
        username = self.user_cbox.itemData(self.user_cbox.currentIndex())
        password = self.password.text()
        could_log_in = authenticate(username, password, self.session)
        if not could_log_in:
            text = '<b><font color=#ff0000>Invalid Password! Please enter correct credentials</font></b>'
        else:
            text = ''
            self.success = True
            self.launch_app.set_user(username)
            self.close()
        self.message_box.setText(text)

    def company_login(self):
        """"""
        admin_dlg = AdminDialog(self.configuration, self)
        admin_dlg.setModal(True)
        admin_ok = admin_dlg.exec_()
        if not admin_dlg.authenticated:
            return
        
        self.launch_app.setup_company_profile_dir()
        self.close()


    def whois_logged_in(self):
        """return the currently logged in user name"""
        user_pref_path_var = self.configuration['application']['canonical_user_pref_location']
        user_pref_path = user_pref_path_var.replace('$username$', os.getlogin())
        if os.path.exists(user_pref_path) and os.path.islink(user_pref_path):
            self.log_out_btn.setEnabled(True)
            if "company-profile" not in os.path.realpath(user_pref_path):
                self.backup_settings_ac.setEnabled(True)
                self.restore_settings_ac.setEnabled(True)
                self.reset_settings_ac.setEnabled(True)
            else:
                self.backup_settings_ac.setEnabled(False)
                self.restore_settings_ac.setEnabled(False)
                self.reset_settings_ac.setEnabled(False)
            self.current_user.setText( os.path.basename(os.path.dirname(os.path.dirname(os.path.realpath(user_pref_path))) )  )
            self.logged_in_menu.setTitle( os.path.basename(os.path.dirname(os.path.dirname(os.path.realpath(user_pref_path))) )  )
        else:
            self.current_user.setText("<default>")
            self.log_out_btn.setEnabled(False)
            self.backup_settings_ac.setEnabled(False)
            self.restore_settings_ac.setEnabled(False)
            self.reset_settings_ac.setEnabled(False)

    def backup_settings(self):
        """"""
        dialog = QtGui.QFileDialog(self, "Select Destination for Backup")
        dialog.setFileMode(QtGui.QFileDialog.Directory)
        dialog.setOption(QtGui.QFileDialog.ShowDirsOnly, True)
        dialog.selectFile("xxxx")
        selected_paths = None
        if dialog.exec_():
            selected_paths = dialog.selectedFiles()
        if selected_paths:
            destination_path = selected_paths[0]
            try:
                self.launch_app.backup_profile_dir(destination_path)
            except Exception as exc:
                err_dlg = QtGui.QMessageBox()
                err_dlg.setText(str(exc))
                err_dlg.setIcon(QtGui.QMessageBox.Critical)
                err_dlg.setStandardButtons(QtGui.QMessageBox.Ok)
                err_dlg.setDefaultButton(QtGui.QMessageBox.Ok)
                err_dlg.exec_()


    def reset_settings(self):
        if not self.authenticate_user():
            return

        err_dlg = QtGui.QMessageBox()
        err_dlg.setText("Are you sure you want to reset your entire profile to the company default?")
        err_dlg.setIcon(QtGui.QMessageBox.Warning)
        err_dlg.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
        err_dlg.setDefaultButton(QtGui.QMessageBox.Cancel)
        return_value = err_dlg.exec_()
        if return_value == QtGui.QMessageBox.Ok:
            destination_path = self.launch_app.company_default_profile
            try:
                self.launch_app.restore_profile_dir(destination_path)
            except Exception as exc:
                err_dlg = QtGui.QMessageBox()
                err_dlg.setText(str(exc))
                err_dlg.setIcon(QtGui.QMessageBox.Critical)
                err_dlg.setStandardButtons(QtGui.QMessageBox.Ok)
                err_dlg.setDefaultButton(QtGui.QMessageBox.Ok)
                err_dlg.exec_()


    def restore_settings(self):
        """"""
        if not self.authenticate_user():
            return
        dialog = QtGui.QFileDialog(self, "Select Profile Directory")
        dialog.setFileMode(QtGui.QFileDialog.Directory)
        dialog.setOption(QtGui.QFileDialog.ShowDirsOnly, True)
        dialog.selectFile("xxxx")
        selected_paths = None
        if dialog.exec_():
            selected_paths = dialog.selectedFiles()
        if selected_paths:
            destination_path = selected_paths[0]
            user_pref_path_var = self.configuration['application']['canonical_user_pref_location']
            user_pref_path = user_pref_path_var.replace('$username$', os.getlogin())
            if os.path.basename(os.path.dirname(os.path.dirname(os.path.realpath(user_pref_path))) ) != destination_path.split(os.path.sep)[-1]:
                err_dlg = QtGui.QMessageBox()
                err_dlg.setText("profile to restore doesn't match logged in user!")
                err_dlg.setIcon(QtGui.QMessageBox.Critical)
                err_dlg.setStandardButtons(QtGui.QMessageBox.Ok)
                err_dlg.setDefaultButton(QtGui.QMessageBox.Ok)
                err_dlg.exec_()
            else:
                try:
                    self.launch_app.restore_profile_dir(destination_path)
                except Exception as exc:
                    err_dlg = QtGui.QMessageBox()
                    err_dlg.setText(str(exc))
                    err_dlg.setIcon(QtGui.QMessageBox.Critical)
                    err_dlg.setStandardButtons(QtGui.QMessageBox.Ok)
                    err_dlg.setDefaultButton(QtGui.QMessageBox.Ok)
                    err_dlg.exec_()


    def logout_user(self):
        """"""
        self.launch_app.reset_profile_dir()
        self.whois_logged_in()

    def set_app(self, application):
        """"""
        self.launch_app = application

    def authenticate_user(self):
        username = self.current_user.text()
        dlg = UserLoginDialog(username, self.session, self)
        dlg.setModal(True)
        admin_ok = dlg.exec_()
        if not dlg.authenticated:
            return False
        return True



class UserLoginDialog(QtGui.QDialog):
    def __init__(self, username, session, parent=None):
        super(UserLoginDialog,self).__init__(parent)
        self.setWindowTitle("Authenticate User")

        self.username = username
        self.session = session
        self.main_lyt = QtGui.QVBoxLayout(self)

        self.user_msg = QtGui.QLabel()
        self.user_msg.setStyleSheet("color: #ff0000")

        self.message = QtGui.QLabel("Please enter user password for %s" % self.username)
        self.password = QtGui.QLineEdit()
        self.password.setPlaceholderText("<enter password>")
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        self.ok_btn = QtGui.QPushButton("OK")

        self.main_lyt.addWidget(self.user_msg)
        self.main_lyt.addWidget(self.message)
        self.main_lyt.addWidget(self.password)
        self.main_lyt.addWidget(self.ok_btn)
        
        self.authenticated = False

        self.ok_btn.clicked.connect(self.authenticate)

    def authenticate(self):
        could_log_in = authenticate(self.username, self.password.text(), self.session)
        if could_log_in:
            self.authenticated = True
            self.close()
        else:
            self.authenticated = False
            self.message.setText("Incorrect Password, please try again!")


class AdminDialog(QtGui.QDialog):
    def __init__(self, configuration, parent=None):
        super(AdminDialog,self).__init__(parent)
        self.setWindowTitle("Administration")
        self.configuration = configuration
        self.main_lyt = QtGui.QVBoxLayout(self)

        self.user_msg = QtGui.QLabel()
        self.user_msg.setStyleSheet("color: #ff0000")

        self.message = QtGui.QLabel("Please enter admin password")
        self.password = QtGui.QLineEdit()
        self.password.setPlaceholderText("<enter password>")
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        self.ok_btn = QtGui.QPushButton("OK")

        self.main_lyt.addWidget(self.user_msg)
        self.main_lyt.addWidget(self.message)
        self.main_lyt.addWidget(self.password)
        self.main_lyt.addWidget(self.ok_btn)
        
        self.authenticated = False

        self.ok_btn.clicked.connect(self.authenticate)

    def authenticate(self):
        password = self.password.text()
        admin_pass = self.configuration['administration']['password']
        if password == admin_pass:
            self.authenticated = True
            self.close()
        else:
            self.authenticated = False
            self.message.setText("Incorrect Password, please try again!")

    def add_current_user(self, username):
        self.user_msg.setText("user's profile to overwrite: %s" % username)

class AddUserDialog(QtGui.QDialog):
    def __init__(self, session, parent=None):
        super(AddUserDialog,self).__init__(parent)
        self.setWindowTitle("Add New User")
        self.session = session

        self.first_name = QtGui.QLineEdit()
        self.last_name = QtGui.QLineEdit()
        self.login_name = QtGui.QLineEdit()
        self.password = QtGui.QLineEdit()
        self.confirm_password = QtGui.QLineEdit()
        self.cancel_btn = QtGui.QPushButton("Cancel")
        self.ok_btn = QtGui.QPushButton("OK")
        self.message_box = QtGui.QLabel()

        self.password.setPlaceholderText("<enter password>")
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        self.confirm_password.setPlaceholderText("<repeat password>")
        self.confirm_password.setEchoMode(QtGui.QLineEdit.Password)
        
        self.ok_btn.setDefault(True)
        self.cancel_btn.clicked.connect(self.reject)
        self.ok_btn.clicked.connect(self.accept)
        
        self.first_name.textChanged.connect(self.check)
        self.last_name.textChanged.connect(self.check)
        self.login_name.textChanged.connect(self.check)
        self.password.textChanged.connect(self.check)
        self.confirm_password.textChanged.connect(self.check)

        self.check()

        self.main_lyt = QtGui.QFormLayout(self)
        self.main_lyt.addRow("First Name", self.first_name)
        self.main_lyt.addRow("Last Name", self.last_name)
        self.main_lyt.addRow("Login Username", self.login_name)
        self.main_lyt.addRow("Password", self.password)
        self.main_lyt.addRow("Confirm Password", self.confirm_password)
        self.main_lyt.addRow(self.cancel_btn, self.ok_btn)
        self.main_lyt.addRow(self.message_box)

    def check(self):
        if self.check_empty() and self.check_password():
            self.ok_btn.setEnabled(True)
        else:
            self.ok_btn.setDisabled(True)
        
    def check_empty(self):
        if self.first_name.text() and self.last_name.text() and self.login_name.text():
            if self.session.query(User).filter(User.login_name == self.login_name.text()).all():
                self.message_box.setText("login name already in use.")
                self.message_box.setStyleSheet("color: #ff0000")
            else:
                self.message_box.setText("")
                return True
        else:
            return False
    
    def check_password(self):
        if self.password.text() == self.confirm_password.text():
            return True
        else:
            return False
    
    def return_user(self):
        return dict(
                    first_name=self.first_name.text(),
                    last_name=self.last_name.text(),
                    login_name=self.login_name.text(),
                    password=self.password.text()
                    )
                    
class RemoveUserDialog(QtGui.QDialog):
    def __init__(self, session, parent=None):
        super(RemoveUserDialog,self).__init__(parent)
        self.setWindowTitle("Remove Users")
        self.setModal(True)
        
        self.session = session
        
        self.main_lyt = QtGui.QVBoxLayout(self)

        self.user_list = QtGui.QListWidget()
        self.delete_users_btn = QtGui.QPushButton("Delete Selected Users")
        
        self.main_lyt.addWidget(self.user_list)
        self.main_lyt.addWidget(self.delete_users_btn)
        
        self.fill_users(self.user_list)
        self._setup_connections()
        
    def _setup_connections(self):
        self.delete_users_btn.clicked.connect(self.delete_checked)
        
    def fill_users(self, widget):
        widget.clear()
        for user in self.session.query(User).all():
            item = QtGui.QListWidgetItem("%s %s (%s)" % (user.first_name, user.last_name, user.login_name))
            item.setData(QtCore.Qt.UserRole, user)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
            item.setCheckState(QtCore.Qt.Unchecked)
            widget.addItem(item)
    
    def delete_checked(self):
        marked_items = list()
        deletion_ids = list()
        deletion_message = ""
        for idx in range(self.user_list.count()):
            item = self.user_list.item(idx)
            if item.checkState():
                marked_items.append(item)
                db_id = item.data(QtCore.Qt.UserRole)
                deletion_ids.append(db_id)
                deletion_message = deletion_message + "\n" + item.text()
        if len(marked_items) == 0:
            return
        
        delete_dlg = QtGui.QMessageBox()
        delete_dlg.setText("Are you Sure you want to delete the following Users?")
        delete_dlg.setIcon(QtGui.QMessageBox.Question)
        delete_dlg.setInformativeText(deletion_message)
        delete_dlg.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
        delete_dlg.setDefaultButton(QtGui.QMessageBox.Cancel)
        return_value = delete_dlg.exec_()

        if return_value == QtGui.QMessageBox.Ok:
            for deletion_id in deletion_ids:
                self.session.delete(deletion_id)
            self.session.commit()
        self.close()
        
