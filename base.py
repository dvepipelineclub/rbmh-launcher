import os
import json

LOCKFILE = 'lock.lock'

def _lockfile():
    here = os.path.abspath(os.path.dirname(__file__))
    return os.path.join(here, LOCKFILE)

def load_config():
    here = os.path.abspath(os.path.dirname(__file__))
    config_json = os.path.join(here, 'configuration.json')
    with open(config_json) as fp:
        loaded = json.load(fp)
        return loaded
        
def check_lock():
    """For the purpose of making only one instance of this program run at a time
    a lockfile is placed. On startup the program must check if this file is set.
    @return True if the lock exists, False otherwise"""
    if os.path.exists(_lockfile()):
        return False
    else:
        return True

def create_lock():
    fp = open(_lockfile(), 'w+')

def clear_lock():
    os.remove(_lockfile())
    
