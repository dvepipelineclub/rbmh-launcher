"""
This module uses sql-alchemy and the associated sql-alchemy-utils ( with passlib )
to setup the user/pass combinations in the database defined in an external json
configuration file. The passwords are stored as hashes in the database for an
additional layer of security.
"""
import sqlalchemy

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy_utils.types import PasswordType

from base import load_config

conf = load_config()

database_type =conf["database"]["dbtype"]
database_host =conf["database"]["hostname"]
database_port = conf["database"]["port"]
database_user = conf["database"]["username"]
database_pass = conf["database"]["password"]
database_name = conf["database"]["dbname"]

def initialize_db():
    from sqlalchemy import create_engine
    engine = create_engine('sqlite:///test.db', echo=False)

    db_cred = "%s%s"
    if database_user:
        db_cred = "%s"
        if database_pass:
            db_cred = db_cred + ":%s"
        db_cred = db_cred + "@"
    
    db_url = "%s%s"
    if database_host and database_port:
        db_url = "%s:%s/"
    db_engine_spec = "%s:///"+db_cred+db_url+"%s"
    engine = create_engine(db_engine_spec % (
                                                    database_type,
                                                    database_user,
                                                    database_pass,
                                                    database_host,
                                                    database_port,
                                                    database_name))
    Base.metadata.create_all(engine) 
    return engine

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    login_name = Column(String, unique=True)
    password = Column(PasswordType(schemes=['sha256_crypt']))


def authenticate(username, password, session):
        user = session.query(User).filter(User.login_name == username).one()
        if user.password != password:
                return False
        return True
