"""
This is a setup.py script generated by py2applet

Usage:
    python setup.py py2app
"""

from setuptools import setup, find_packages

APP = ['launcher.py']
DATA_FILES = ['configuration.json', 'red-bull-media-house-logo.png', 'users.db']
OPTIONS = {'argv_emulation': True, 'frameworks':['QtCore.framework', 'QtGui.framework'], 'includes':['PySide', 'PySide.QtCore','PySide.QtGui'], 'iconfile':'icon.icns', 'plist':{'CFBundleShortVersionString':'0.9.2', 'CFBundleVersion':'0.9.2'}}
PY_MODULES = ['authentication', 'base', 'gui', 'app_launch']
PACKAGES= ['libs.sqlalchemy', 'libs.sqlalchemy_utils', 'libs.passlib', 'libs.sqlalchemy.orm', 'libs.six']

setup(
    name="Adobe Premiere Profile Sync",
    app=APP,
    data_files=DATA_FILES,
    py_modules=PY_MODULES,
    package_dir={"":"libs"},
    packages=find_packages('libs'),
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
)
