"""
"""
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), 'libs'))
sys.path.append(os.path.join(os.path.dirname(__file__), 'libs_pyside'))

from PySide import QtCore, QtGui

from gui import (LauncherWindow,
                 show_error_message
                )
from app_launch import Application
from base import (load_config, 
                  check_lock, 
                  create_lock, 
                  clear_lock
                 )

if __name__ == "__main__":
    config = load_config()
    launch_app = Application(config)
    app = QtGui.QApplication(sys.argv)
    
    # This vestigial locking is not really required as an open instance
    # of premiere would be used instead of starting a new one
    # However it can lead to user fatigue if the lock needs to be removed
    # so this part is no longer active
    #if not check_lock():
    #    show_error_message("Warning", "an instance of this application is already running. Please close it before continuing.")
    #    sys.exit()
    
    #create_lock()

    win = LauncherWindow(config)
    win.set_app(launch_app)
    win.show()
    
    sys_ret = app.exec_()

    if win.success and not sys_ret:
        launch_app.launch()
    
    #clear_lock()
    sys.exit()
