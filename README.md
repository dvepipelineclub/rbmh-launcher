PREREQUISITES
=============

_For added convenience, get-pip.py is included in the repository_

1. Qt Framework: OSX Binaries can be downloaded http://download.qt-project.org/archive/qt/4.8/4.8.5/qt-mac-opensource-4.8.5.dmg
2. PySide: OSX Binaries can be downloaded http://pyside.markus-ullmann.de/pyside-1.2.1-qt4.8.5-py27apple-developer-signed.pkg

For installation instructions of PySide, see the page here:
https://github.com/PySide/pyside-setup

INSTALLATION
============

1. Install Qt from the .dmg installer located in the base directory
2. 


Installation TODOs
==================

* Where should the MediaCache/Files registry keys point to?
* Where should the user database be placed?
* Which keys should be overridden per User?
* Change the Admin Password
* Administrator needs to place the Adobe Pref Config File in /Library/Application Support/Adobe/Adobe Premiere Pro CC 2014/....



Premiere Pro Preference Location Listings
=========================================

## 1. /Users/<username>/Documents/Adobe/Premiere Pro/8.0
If this directory doesn't exist, it is created on startup of Premiere
on the fly and populated with default settings
* AMERequestDB				
* Adobe Premiere Pro Preview Files	
* <project-name>.prproj
* Profile-<username>
 * Adobe Premiere Pro Prefs		
 * Media Browser Provider Exception
 * ArchivedLayouts			
 * Recent Directories
 * Effect Presets and Custom Items.prfpset	
 * Settings
  * Source Patcher Presets/
  * Overlay Presets/
  * Track Height Presets/ 
 * Layouts					
  * UserWorkspace1.xml   
  * WorkspaceConfig.xml
 * SharedView Column Settings
 * Mac

## 2. /Users/<username>/Library/Application\ Support/Adobe/Premiere\ Pro/8.0/
* Plugin Loading.log

## 3. /Users/<username>/Library/Preferences/Adobe/Premiere\ Pro/8.0/
* AudioPluginManagerSettings.xml
* PresetCache.xml
* ControlSurface			
* Trace Database.txt
* Debug Database.txt		
* logs

## 4. /Library/Application Support/Adobe/Adobe Premiere Pro CC 2014
* Legal

## 5. MediaCaches -- /Users/andreas.gutsche/Library/Application\ Support/Adobe/Common/

* Media Cache				
* MediaCoreQTCodecRulesCC 2014.xml
* Media Cache Files			
* PTX

Adobe Premiere Configuration Artifacts
======================================

* Preferences / Settings
 * General
* Workspace Layout
* Keyboard Shortcuts


01_Adobe_Media_Encoder
=====================
goes into <user>/Documents/Adobe Media Encoder/8.0

02_Profile_User_Settings
===================
goes into <user>/Documents/Premiere Pro/8.0
-- Don’t forget to change the User of the Machine for the Creative cloud, delete toms User

03_AME_User_Presets
==================
<user>/Library/Preferences/Adobe/Common/AME/8.0
Don’t forget to Make a new Alias, out of 01 Presets
…> symlink to 01_AME

04_Audio_Channel_Presets
=====================
<user>/Library/Application Support/Adobe/Common/8.0

