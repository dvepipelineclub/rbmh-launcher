"""
"""
import os
import shutil
import time
import subprocess

class Application(object):
    def __init__(self, configuration):
        self.configuration = configuration

        self.user_pref_path_var = self.configuration['application']['canonical_user_pref_location']
        self.app_path = self.configuration['application']['path']
        self.custom_prefs_root = self.configuration['filesystem']['root']
        self.company_default_profile = os.path.join(self.custom_prefs_root, 'company-profile')

        audio_channel_preset_path = self.configuration['application']['audio_channel_preset_path']
        ame_user_pref_path = self.configuration['application']['ame_user_pref_path']
        media_encoder_pref_path = self.configuration['application']['media_encoder_pref_path']

        self.audio_channel_preset_path = audio_channel_preset_path.replace('$username$', os.getlogin())
        self.ame_user_pref_path = ame_user_pref_path.replace('$username$', os.getlogin())
        self.media_encoder_pref_path = media_encoder_pref_path.replace('$username$', os.getlogin())
        self.premiere_path = self.user_pref_path()


    def user_pref_path(self):
        return self.user_pref_path_var.replace('$username$', os.getlogin())

    def launch(self):
        self.setup()
        # no longer need to launch application, just
        # setup user.
        #subprocess.Popen(['open', '-a', app_path])
    
    def set_user(self, username):
        """"""
        self.username = username

    def setup(self):
        self.setup_profile_dir()
        #self.setup_external_pref_config()
        #self.setup_user_preference_override()
    def current_user_profile_path(self, from_dir):
        common_prefix = os.path.commonprefix([self.custom_prefs_root, from_dir])
        if common_prefix:
            relative_path = os.path.relpath(from_dir, self.custom_prefs_root)
            if len(relative_path.split(os.path.sep)) > 1:
                return os.path.join(common_prefix, *relative_path.split(os.path.sep)[:2])
        return None

    def current_user_profile(self, from_dir):
        user_path = self.current_user_profile_path(from_dir)
        if user_path:
            return len(user_path.split(os.path.sep)) > 1 and user_path.split(os.path.sep)[-1]

    def backup_profile_dir(self, destination_path):
        if os.path.exists(self.user_pref_path()) and os.path.islink(self.user_pref_path()):
            fullpath = os.path.realpath(self.user_pref_path())
            user_path = self.current_user_profile_path(fullpath)
            username = self.current_user_profile(fullpath)
            shutil.copytree(user_path, os.path.join(destination_path, username), symlinks=True)

    def restore_profile_dir(self, source_path):
        if os.path.exists(self.current_user_profile_path(os.path.realpath(self.user_pref_path()) ) ):
            fullpath = os.path.realpath(self.user_pref_path())
            user_path = self.current_user_profile_path(fullpath)
            username = self.current_user_profile(fullpath)
            shutil.rmtree(user_path)
            time.sleep(0.5)
            shutil.copytree(source_path, user_path)

    def reset_profile_dir(self):
        path_list = (self.audio_channel_preset_path,
        self.ame_user_pref_path,
        self.media_encoder_pref_path,
        self.user_pref_path(),
        os.path.join(os.path.dirname(self.premiere_path), 'AMERequestDB'),
        os.path.join(os.path.dirname(self.premiere_path), 'Profile Config')
        )
        for path in path_list:
            if os.path.exists(path) and os.path.islink(path):
                os.remove(path)
                time.sleep(0.1)
                shutil.move(path + ".origin", path)


    def _init_user_profile(self, username):
        user_pref_src = os.path.join(self.custom_prefs_root, 'profiles', username)
        if not os.path.exists(user_pref_src):
            shutil.copytree(self.company_default_profile, user_pref_src)
            preset_link = os.path.join(user_pref_src, "ame_user_preferences", "Presets")
            #preset_link = os.path.join(user_pref_src, "ame_user_preferences", "AME", "8.0", "Presets")
            media_encoder_dir = os.path.join("..", "media_encoder_presets", "Presets")
            #media_encoder_dir = os.path.join("..","..","..", "media_encoder_presets", "8.0", "Presets")
            os.symlink(media_encoder_dir, preset_link)

    def _link_folder(self, source_folder, target_folder):
        if os.path.exists(target_folder):
            if os.path.islink(target_folder):
                os.remove(target_folder)
            else:
                shutil.move(target_folder, target_folder + ".origin")
        elif os.path.islink(target_folder):
            os.remove(target_folder)
        else:
            target_dir = os.path.dirname(target_folder)
            if not os.path.exists(target_dir):
                time.sleep(0.1)
                os.makedirs(target_dir)
            #shutil.move(target_folder, target_folder + ".origin")

        time.sleep(0.2)
        os.symlink(source_folder, target_folder)


    def setup_profile_dir(self):
        user_pref_src = os.path.join(self.custom_prefs_root, 'profiles', self.username)
        self._init_user_profile(self.username)
        #self._link_folder(user_pref_src, self.user_pref_path())
        self._setup_all_adobe_links(user_pref_src)
    
    def setup_company_profile_dir(self):
        #self._link_folder(self.company_default_profile, self.user_pref_path())
        self._setup_all_adobe_links(self.company_default_profile)

    def _setup_all_adobe_links(self, custom_pref_source):
        #Adobe Premiere Configuration Artifacts
        #======================================
        #
        #* Preferences / Settings
        # * General
        #* Workspace Layout
        #* Keyboard Shortcuts


        #01_Adobe_Media_Encoder
        #=====================
        #goes into <user>/Documents/Adobe Media Encoder/8.0

        ame_prefs = os.path.join(custom_pref_source,'ame_user_preferences')
        audio_presets = os.path.join(custom_pref_source,'audio_channel_presets')
        media_encoder = os.path.join(custom_pref_source,'media_encoder_presets')
        premiere_prefs = os.path.join(custom_pref_source,'premiere_profile', 'user_profile')
        
        premiere_amereq = os.path.join(custom_pref_source,'premiere_profile', 'AMERequestDB')
        premiere_profile_cache = os.path.join(custom_pref_source,'premiere_profile', 'Profile Config')

        self._link_folder(ame_prefs, self.ame_user_pref_path)
        self._link_folder(audio_presets, self.audio_channel_preset_path)
        self._link_folder(media_encoder, self.media_encoder_pref_path)
        self._link_folder(premiere_prefs, self.premiere_path)
        
        self._link_folder(premiere_amereq, os.path.join(os.path.dirname(self.premiere_path), 'AMERequestDB'))
        self._link_folder(premiere_profile_cache, os.path.join(os.path.dirname(self.premiere_path), 'Profile Config'))

        #02_Profile_User_Settings
        #===================
        #goes into <user>/Documents/Premiere Pro/8.0
        #-- Don't forget to change the User of the Machine for the Creative cloud, delete toms User

        #03_AME_User_Presets
        #==================
        #<user>/Library/Preferences/Adobe/Common/AME/8.0
        #Don't forget to Make a new Alias, out of 01 Presets
        #> symlink to 01_AME

        #04_Audio_Channel_Presets
        #=====================
        #<user>/Library/Application Support/Adobe/Common/8.0
