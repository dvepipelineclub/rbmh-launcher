import os

def run_check():
	path_dict = {"canonical_user_pref_location": "/Users/$username$/Documents/Adobe/Premiere Pro/8.0/Profile-$username$",
	"audio_channel_preset_path": "/Users/$username$/Library/Application Support/Adobe/Common/8.0",
	"ame_user_pref_path": "/Users/$username$/Library/Preferences/Adobe/Common/AME/8.0",
	"media_encoder_pref_path": "/Users/$username$/Documents/Adobe/Adobe Media Encoder/8.0",
	"amereq": "/Users/$username$/Documents/Adobe/Premiere Pro/8.0/AMERequestDB",
	"profilecache":"/Users/$username$/Documents/Adobe/Premiere Pro/8.0/Profile Config"
	}
	for key, value in path_dict.iteritems():
		path = value.replace("$username$", os.getlogin())
		if os.path.exists(path):
			if os.path.islink(path):
				print "FAIL\n\tfor path: %s" % path
				print "\tsymlinks to %s" % os.path.realpath(path)
			else:
				print "\t\t\t--OK for %s" % path
		else:
			print "NO PATH EXISTS for %s" % path
		if os.path.exists(path + ".origin"):
			print "FAIL\n\tvestigial path %s" % path+".origin"

if __name__ == '__main__':
	run_check()